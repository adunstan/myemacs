My Emacs setup
==============

download from https://bitbucket.org/adunstan/myemacs/get/master.tar.gz
or https://bitbucket.org/adunstan/myemacs/get/master.zip

On Windows this goes in %APPDATA% (usually C:\Users\<username>\AppData\Roaming)
