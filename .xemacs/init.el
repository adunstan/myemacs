;; Red Hat Linux default .emacs initialization file

;; Are we running XEmacs or Emacs?

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

(defvar running-xemacs (string-match "XEmacs\\|Lucid" emacs-version))

;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)

;; Turn on font-lock mode for Emacs
(cond ((not running-xemacs)
       (global-font-lock-mode t)
))

(setq vc-handled-backends nil)

;; Visual feedback on selections
(setq-default transient-mark-mode t)

;; Always end a file with a newline
(setq require-final-newline t)

;; Stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

;; Enable wheelmouse support by default
(cond (window-system
       (mwheel-install)
))


(setq inhibit-startup-message t) 
(setq initial-scratch-message nil)


;;(require 'cc-align)
;;
(c-add-style "pgsql"
             '("bsd"
               (c-auto-align-backslashes . nil)
               (c-basic-offset . 4)
               (c-offsets-alist . ((case-label . +)
                                   (label . -)
                                   (statement-case-open . +)))
               (fill-column . 78)
               (indent-tabs-mode . t)
               (tab-width . 4))
	nil)

;;(c-add-style "pgsql"
;;                      '("bsd"
;;                                 (indent-tabs-mode . t)
;;                                 (c-basic-offset   . 4)
;;                                 (tab-width . 4)
;;                                 (c-offsets-alist .
;;                                            ((case-label . +)))
;;                      )
;;                      nil ) ; t = set this mode, nil = don't

(defun pgsql-c-mode ()
  (c-mode)
  (c-set-style "pgsql")
)

(setq auto-mode-alist
;;              (cons '("\\`/home/andrew/.*/pgsql.*/.*\\.[chyl]\\'" . pgsql-c-mode)
              (cons '("\\`.*/pg.*/.*\\.[chyl]\\'" . pgsql-c-mode)
            auto-mode-alist))
(setq auto-mode-alist (cons '("\\.[a-z]?html?[a-z]?$" . html-mode) auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Mode for Perl files to match src/tools/pgindent/perltidyrc formatting

(defun pgsql-perl-style ()
  "Perl style adjusted for PostgreSQL project"
  (interactive)
  (setq perl-brace-imaginary-offset 0)
  (setq perl-brace-offset 0)
  (setq perl-continued-statement-offset 2)
  (setq perl-continued-brace-offset (- perl-continued-statement-offset))
  (setq perl-indent-level 4)
  (setq perl-label-offset -2)
  (setq perl-indent-continued-arguments 4)
  (setq perl-indent-parens-as-block t)
  (setq cperl-brace-imaginary-offset 0)
  (setq cperl-brace-offset 0)
  (setq cperl-continued-statement-offset 2)
  (setq cperl-continued-brace-offset (- cperl-continued-statement-offset))
  (setq cperl-indent-level 4)
  (setq cperl-label-offset -2)
  (setq cperl-indent-continued-arguments 4)
  (setq cperl-indent-parens-as-block t)
  (setq indent-tabs-mode t)
  (setq tab-width 4))

(add-hook 'perl-mode-hook
           (lambda ()
             (if (string-match "postgresql" buffer-file-name)
                 (pgsql-perl-style))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key [f9]  'pgsql-perl-style)
;; (global-set-key [f10] 'pgsql-c-mode)
;; f10 is the menu access key
(global-set-key [f11] 'kill-this-buffer)
(global-set-key [f12] 'goto-line)
(global-set-key [f2] 'undo)

;; Creating a new menu pane in the menu bar to the right of Tools menu
(define-key-after
  global-map
  [menu-bar mymenu]
  (cons "AMD" (make-sparse-keymap "hoot hoot"))
  'tools )

(define-key
  global-map
  [menu-bar mymenu un]
  '("Undo (F2)" . undo))

(define-key
  global-map
  [menu-bar mymenu psqlpl]
  '("PGSQL Perl style (F9)" . pgsql-perl-style))

(define-key
  global-map
  [menu-bar mymenu psqlc]
  '("PGSQL C style" . pgsql-c-mode))

(define-key
  global-map
  [menu-bar mymenu kbuf]
  '("Kill Buffer (F11)" . kill-this-buffer))

(define-key
  global-map
  [menu-bar mymenu gotoln]
  '("Go To Line (F12)" . goto-line))


;; (define-key
;;  global-map
;;  [menu-bar mymenu zoomin]
;;  '("Zoom In C-X C-+" . text-scale-increase))

;;;(define-key
;;  global-map
;;  [menu-bar mymenu zoomout]
;;  '("Zoom out C-X C--" . text-scale-decrease))

; make Ctrl-tab insert a tab
(global-set-key (kbd "<C-tab>") 'tab-to-tab-stop)

(setq load-path
      (cons "~/.xemacs" load-path))
 (autoload 'tt-mode "tt-mode")
 (setq auto-mode-alist
  (append '(("\\.tt$" . tt-mode))  auto-mode-alist ))

(defun pgsql-sgml-mode ()
  "SGML mode adjusted for PostgreSQL project"
  (interactive)
  (sgml-mode)

  (setq indent-tabs-mode nil)
  (setq sgml-basic-offset 1)
)

(setq auto-mode-alist
  (cons '("\\(postgres\\|pgsql\\|pg_\\).*\\.sgml\\'" . pgsql-sgml-mode)
        auto-mode-alist))

;;; cperl-mode is preferred to perl-mode                                        
;;; "Brevity is the soul of wit" <foo at acm.org>                               
(defalias 'perl-mode 'cperl-mode)

(setq cperl-invalid-face nil) 
(defun ad-perl-indent-setup ()
  (setq cperl-brace-imaginary-offset 0)
  (setq cperl-brace-offset 0)
  (setq cperl-continued-statement-offset 2)
  (setq cperl-continued-brace-offset (- cperl-continued-statement-offset))
  (setq cperl-indent-level 4)
  (setq cperl-label-offset -2)
  (setq cperl-indent-continued-arguments 4)
  (setq cperl-indent-parens-as-block t)

  (setq cperl-extra-newline-before-brace t)
;;  (setq cperl-indent-level 4)
;;  (setq cperl-brace-offset -2)
  (hs-minor-mode)
  (cperl-set-style "bsd"))

(add-hook 'cperl-mode-hook 'ad-perl-indent-setup)


(defun ad-sql-hook ()
  (setq tab-width 4)
  (setq indent-tabs-mode nil))
(add-hook 'sql-mode-hook 'ad-sql-hook)


(require 'cl-lib)
(require 'git-commit)
(add-hook 'git-commit-mode-hook 'turn-on-flyspell)
(add-hook 'git-commit-mode-hook (lambda () (toggle-save-place 0)))



;; (add-to-list 'load-path "/home/andrew/.xemacs/rust")
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

;;;(require 'mmm-mode)

;;;(mmm-add-classes
;;; '((sql-embedded-perl
;;;    :submode CPerl-mode
;;;	:face mmm-declaration-submode-face
;;;    :front "^-- <perl>"
;;;    :back "^-- </perl>")))

;;;(setq mmm-global-mode 't)
;;;(add-to-list 'mmm-mode-ext-classes-alist '(SQL[ansi]-mode nil sql-embedded-perl))
;; (mmm-add-mode-ext-class 'SQL[ansi] nil 'sql-embedded-perl)
;;
;(require 'pgef)


(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))


(setq vc-make-backup-files t)

(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      delete-by-moving-to-trash t
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )

;; make these dirs if they don't exist, do nothing if they do
(make-directory "~/.saves" t)
(make-directory "~/.auto-recover" t)

(setq backup-directory-alist `(("." . "~/.saves")))
(setq auto-save-file-name-transforms `((".*" ,"~/.auto-recover/" t)))
(setq create-lockfiles nil)

(setq visible-bell t)

;; enabling this can cause hangups, so disable it
(setq x-select-enable-clipboard-manager nil)

;; melpa

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))

(require 'use-package)

(use-package editorconfig
  :ensure t)

;; (use-package git-commit
;;  :ensure t
;;  :init
;;  (global-git-commit-mode)
;;  )

(editorconfig-mode 1)
;;;

(require 'recentf)
(recentf-mode 1)

;; (use-package meson-mode :ensure t)

;; turn on column number status
(setq column-number-mode t)


