(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(inhibit-startup-screen t)
 '(line-number-mode t)
 '(package-selected-packages '(use-package meson-mode git-commit editorconfig))
 '(perl-label-offset 0)
 '(query-user-mail-address nil)
 '(tab-width 4)
 '(toolbar-captioned-p t)
 '(user-mail-address "andrew@dunslane.net"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;;(c-add-style "pgsql"
;;                      '("bsd"
;;                                 (indent-tabs-mode . t)
;;                                 (c-basic-offset   . 4)
;;                                 (tab-width . 4)
;;                                 (c-offsets-alist .
;;                                            ((case-label . +)))
;;                      )
;;                       t) ; t = set this mode on

;;(setq auto-mode-alist
;;              (cons '("\\`/home/andrew/pgsql/.*\\.[chyl]\\'" . pgsql-c-mode)
;;            auto-mode-alist))
