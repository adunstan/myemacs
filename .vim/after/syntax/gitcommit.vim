syn match   gitcommitSummary    "^.\{0,64\}" contained containedin=gitcommitFirstLine nextgroup=gitcommitOverflow contains=@Spell
setlocal formatoptions-=l
